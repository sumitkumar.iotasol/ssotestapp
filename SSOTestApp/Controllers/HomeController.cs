﻿using SSOTestApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Web;
using System.Web.Mvc;

namespace SSOTestApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult LogIn()
        {
            ViewBag.Title = "Login Page";

            return View();
        }

        public ActionResult UserBasicDetails()
        {
            UserProfileModel userDetails = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //HTTP GET
                var responseTask = client.GetAsync("user/details");
                responseTask.Wait();

                var mailResult = responseTask.Result;
                if (mailResult.IsSuccessStatusCode)
                {
                    var readTask = mailResult.Content.ReadAsAsync<UserProfileModel>();
                    readTask.Wait();

                    userDetails = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }

            return View(userDetails);
        }

        public ActionResult Mailbox()
        {
            IEnumerable<UserEmailModel> mails = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //HTTP GET
                var responseTask = client.GetAsync("user/emails");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<UserEmailModel>>();
                    readTask.Wait();

                    mails = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    mails = Enumerable.Empty<UserEmailModel>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(mails);
        }

        public ActionResult MailboxView(string id)
        {
            EmailPreviewModel mailsPreview = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //HTTP GET
                var responseTask = client.GetAsync("user/email/"+id);
                responseTask.Wait();

                var mailResult = responseTask.Result;
                if (mailResult.IsSuccessStatusCode)
                {
                    var readTask = mailResult.Content.ReadAsAsync<EmailPreviewModel>();
                    readTask.Wait();

                    mailsPreview = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            
            return View(mailsPreview);
        }

        public ActionResult Calendar()
        {
            IEnumerable<CalendarDetailModel> calendarList = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //HTTP GET
                var responseTask = client.GetAsync("user/calendars");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<CalendarDetailModel>>();
                    readTask.Wait();

                    calendarList = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    calendarList = Enumerable.Empty<CalendarDetailModel>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(calendarList);
        }
        public ActionResult OneDrive()
        {
            IEnumerable<UserDriveModel> fileList = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //HTTP GET
                var responseTask = client.GetAsync("user/drive");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<UserDriveModel>>();
                    readTask.Wait();

                    fileList = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    fileList = Enumerable.Empty<UserDriveModel>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(fileList);
        }

        public ActionResult SearchOneDrive(string searchText)
        {
            IEnumerable<UserDriveModel> fileList = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //HTTP GET
                var responseTask = client.GetAsync("search/drive/item/"+ searchText);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<UserDriveModel>>();
                    readTask.Wait();

                    fileList = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    fileList = Enumerable.Empty<UserDriveModel>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(fileList);
        }

        public ActionResult CreateFolder(string folderName)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                HttpContent content = new StringContent(@"{ ""Username"": """ + folderName + @"""}");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //HTTP POST
                var responseTask = client.PostAsync("create/new/folder", content);
                responseTask.Wait();

                //ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                
            }
            return View();
        }

        public ActionResult DriveById(string id)
        {
            IEnumerable<UserDriveModel> fileList = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //HTTP GET
                var responseTask = client.GetAsync("user/drive/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<UserDriveModel>>();
                    readTask.Wait();

                    fileList = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    fileList = Enumerable.Empty<UserDriveModel>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(fileList);
        }

        public ActionResult GetSharePoint()
        {
            var sharePointSite = new SharePointModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //HTTP GET
                var responseTask = client.GetAsync("get/site/root");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<SharePointModel>();
                    readTask.Wait();

                    sharePointSite = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(sharePointSite);
        }

        public ActionResult CreateEvent(CalendarDetailModel model,bool isSave = false)
        {
            using (var client = new HttpClient())
            {
                if (isSave == true)
                {
                    client.BaseAddress = new Uri("https://localhost:44315/api/");
                    var opt = new JsonSerializerOptions() { WriteIndented = true };
                    string strJson = JsonSerializer.Serialize<CalendarDetailModel>(model, opt);
                    HttpContent content = new StringContent(strJson);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    //HTTP POST
                    var responseTask = client.PostAsync("create/calendar/event", content);
                    responseTask.Wait();
                }
            }
            return View();
        }
    }
}
