﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.Graph;
using Newtonsoft.Json;
using SSOTestApp.Models;

namespace SSOTestApp.Api
{
    [RoutePrefix("api")]
    public class OutlookController : ApiController
    {
        public readonly static string ClientId = ConfigurationManager.AppSettings["ClientId"];
        public readonly static string ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
        public readonly static string CallbackUri = ConfigurationManager.AppSettings["CallbackUri"];
        public readonly static string TenantId = ConfigurationManager.AppSettings["TenantId"];
        public readonly static string UserId = ConfigurationManager.AppSettings["UserId"];
        public readonly static string TimeZone = ConfigurationManager.AppSettings["TimeZone"];
        private static Stream fileStream;

        public OutlookController()
        {
        }

        //Get Connected Account URl
        [Route("connect/office/account")]
        [HttpGet]
        public IHttpActionResult GetOfficeAccount()
        {
            try
            {
                var resp = GetAuthUri();
                Uri uri = new Uri(resp);
                return Ok(uri);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Redirect Uri
        [Route("get/token")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAccessToken(string code)
        {
            try
            {
                var resp = await AccessToken(code);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //
        public static string GetAuthUri()
        {
            Random generator = new Random();
            String state = generator.Next(0, 99999).ToString("D5");
            var requestURI = string.Format(
               "https://login.microsoftonline.com/{3}/oauth2/v2.0/authorize?" +
               "client_id={0}" +
               "&response_type=code" +
               "&redirect_uri={1}" +
               "&response_mode=query" +
               "&scope=offline_access user.read calendars.readwrite calendars.readwrite.shared" +
               "&state={2}",
               ClientId,    
               CallbackUri, state, TenantId);

            return requestURI;
        }
        //Generate Acces Token
        public static async Task<BearerTokenModel> AccessToken(string code)
        {
            var values = new Dictionary<string, string>
            {
                { "client_id", ClientId },
                { "scope", "user.read calendars.readwrite calendars.readwrite.shared" },
                { "code", code },
                { "redirect_uri", CallbackUri },
                { "grant_type", "authorization_code" },
                { "client_secret", ClientSecret }
            };
            var requestUri = string.Format("https://login.microsoftonline.com/{0}/oauth2/v2.0/token", TenantId);
            var content = new FormUrlEncodedContent(values);
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(requestUri, content);
            BearerTokenModel model = new BearerTokenModel();
            if (response.IsSuccessStatusCode)
            {
                string responseString = response.Content.ReadAsStringAsync().Result;
                var responseObj = JsonConvert.DeserializeObject<BearerTokenModel>(responseString);
                DateTimeOffset tokenExpireDate = DateTimeOffset.UtcNow.AddSeconds(double.Parse(responseObj.expires_in));
                responseObj.tokenexpireson = tokenExpireDate;
                //var getuserDeatils = await GetUserIdWithToken(responseObj.access_token);
                //responseObj.UserPrincipleName = getuserDeatils;
                StoreToken(JsonConvert.SerializeObject(responseObj));
                return responseObj;
            }
            else
            {                            
                throw new ArgumentException("Failed to get authtoken due response code." + response.StatusCode);
            }
            
        }
        public static async Task<string> GetUserIdWithToken(string token)
        {
            UserProfileModel usermodel = new UserProfileModel();
            if (token == null)
            {
                usermodel = null;
                return "";
            }
            GraphServiceClient graphServiceClient =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", token);
                })
            );

            var response = await graphServiceClient.Me.Request().GetAsync();
            //if (response.State)
            var userId = response.UserPrincipalName;
            return userId;
        }

        //Api for refresh Token
        [Route("refresh/access/token")]
        [HttpGet]
        public async Task<IHttpActionResult> RefreshAccessToken()
        {
            var storedToken = ReadStoredToken();
            var refreshToken = storedToken.refresh_token;
            try
            {
                var resp = refreshToken != null ? await RefreshToken(refreshToken) : null;
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Refresh Token
        public static async Task<BearerTokenModel> RefreshToken(string refreshToken)
        {
            //var _outlookService = EngineContext.Current.Resolve<IOutlookService>();
            var values = new Dictionary<string, string>
            {
                { "client_id", ClientId },
                { "scope", "user.read calendars.readwrite calendars.readwrite.shared" },
                { "refresh_token", refreshToken },
                { "grant_type", "refresh_token" },
                { "client_secret", ClientSecret },
            };

            var requestUri = string.Format("https://login.microsoftonline.com/{0}/oauth2/v2.0/token",TenantId);
            var content = new FormUrlEncodedContent(values);
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(requestUri, content);
            if (response.IsSuccessStatusCode)
            {
                string responseStrings = response.Content.ReadAsStringAsync().Result;
                var reponseObj = JsonConvert.DeserializeObject<BearerTokenModel>(responseStrings);
                DateTimeOffset tokenExpireDate = DateTimeOffset.UtcNow.AddSeconds(double.Parse(reponseObj.expires_in));
                reponseObj.tokenexpireson = tokenExpireDate;
                StoreToken(JsonConvert.SerializeObject(reponseObj));
                return reponseObj;
            }
            else
            {
                throw new ArgumentException("Failed to get authtoken due response code." + response.StatusCode);
            }
        }
        //Store Token as Json object in local drive
       public static void StoreToken(string response)
        {
            string currentPath = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = currentPath + "OutlookToken.json";
            System.IO.File.WriteAllText(filePath, response);
        }
        //Read stored token from json file
        private static BearerTokenModel ReadStoredToken()
        {
            string currentPath = AppDomain.CurrentDomain.BaseDirectory;
            string filePath = currentPath + "OutlookToken.json";
            string serializedToken = System.IO.File.ReadAllText(filePath);
            var token = JsonConvert.DeserializeObject<BearerTokenModel>(serializedToken);
            return token;
        }
        //Get Stored token
        public static async Task<string> GetToken()
        {
            var outlookToken = ReadStoredToken();
            if(outlookToken == null)
            {
                return  null;
            }
            var utcTimeNow = DateTime.UtcNow;
            if (utcTimeNow > outlookToken.tokenexpireson)
            {
                var refreshToken = outlookToken.refresh_token;
                outlookToken = await RefreshToken(refreshToken);
            }
            if (outlookToken.access_token == null)
            {
                return null;
            }
            return outlookToken.access_token;
        }
        //Api to Get Users Details
        [Route("user/details")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserDetails()
        {
            try
            {
                var resp = await GetUserDetailWithToken();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get User Details of O365 profile using token
        public static async Task<UserProfileModel> GetUserDetailWithToken()
        {
            UserProfileModel usermodel = new UserProfileModel();
            var result = await GetToken();
            if (result==null)
            {
                usermodel = null;
                return usermodel;
            }
            GraphServiceClient graphServiceClient =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            
            var httpMessage = graphServiceClient.Me.Request().GetHttpRequestMessage();
            var response = await graphServiceClient.HttpProvider.SendAsync(httpMessage);
            var jsonContent = await response.Content.ReadAsStringAsync();
            usermodel = JsonConvert.DeserializeObject<UserProfileModel>(jsonContent);
            return usermodel;
        }
        //Api to Get User calendars
        [Route("user/calendars")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserCalendar()
        {
            try
            {
                var resp = await GetUserCalendars();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get User calendars using Bearer Token
        public static async Task<List<CalendarDetailModel>> GetUserCalendars()
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );

            var calendars = await graphService.Me.Calendars
                            .Request()
                            .GetAsync();
            List<CalendarDetailModel> calendarList = new List<CalendarDetailModel>();
            foreach(var item in calendars)
            {
                CalendarDetailModel model = new CalendarDetailModel();
                model.CalendarId = item.Id;
                model.CalendarName = item.Name;
                model.Owner = item.Owner.Address;
                calendarList.Add(model);
            }
            return calendarList;
        }
        //Api to Get all Event list from calendars
        [Route("user/calendar/events")]
        [HttpPost]
        public async Task<IHttpActionResult> GetUserCalendarEvents(CalendarDetailModel model)
        {
            try
            {
                var resp = await GetCalendarEvents(model);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get all events from calendar
        public static async Task<List<CalendarDetailModel>> GetCalendarEvents(CalendarDetailModel model)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var queryOptions = new List<QueryOption>()
                            {
                                new QueryOption("startDateTime", model.StartTime.ToString("yyyy-MM-ddT00:00:00")),
                                new QueryOption("endDateTime", model.EndTime.ToString("yyyy-MM-ddT23:59:59")),
                                new QueryOption("timeZome",TimeZone)
                            };
            
            var calendar = await graphService.Me.Calendar.CalendarView                         
                            .Request(queryOptions)
                            .GetAsync();
            List<CalendarDetailModel> eventList = new List<CalendarDetailModel>();
            var attendees = new List<Attendee>();
            foreach (var item in calendar)
            {
                CalendarDetailModel calendarModel = new CalendarDetailModel();
                //calendarModel.EventBodyContent = item.Body.Content;
                calendarModel.StartTime= Convert.ToDateTime(item.Start.DateTime);
                calendarModel.EndTime= Convert.ToDateTime(item.End.DateTime);
                calendarModel.Location= item.Location.DisplayName;
                calendarModel.OrganizerName = item.Organizer.EmailAddress.Name;
                calendarModel.OrganizerEmailAddress = item.Organizer.EmailAddress.Address;
                calendarModel.Subject = item.Subject;
                calendarModel.Id = item.Id;
                /*foreach(var attendeeList in item.Attendees)
                {
                    var attendee = new Attendee();
                    if (attendee.EmailAddress == null)
                    {
                        attendee.EmailAddress = new EmailAddress();
                    }
                    attendee.EmailAddress.Address = attendeeList.EmailAddress.Address;
                    attendee.EmailAddress.Name = attendeeList.EmailAddress.Name;
                    attendees.Add(attendee);
                }
                calendarModel.AttendeeList = attendees;*/
                eventList.Add(calendarModel);
            }
            return eventList;
        }
        //Api to get Users Busy/Free Schedule 
        [Route("user/schedule")]
        [HttpPost]
        public async Task<IHttpActionResult> GetUsersScehdule(CalendarDetailModel model)
        {
            try
            {
                var resp = await GetUserScehdule(model);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get User Schedule 
        public static async Task<ICalendarGetScheduleCollectionPage> GetUserScehdule(CalendarDetailModel model)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var schedules = model.UsersId != null ? model.UsersId.Select(x => x.Email.ToString()).ToList() : Enumerable.Empty<string>();
            var startTime = new DateTimeTimeZone
            {
                DateTime = model.StartDateTime,
                TimeZone = TimeZone
            };

            var endTime = new DateTimeTimeZone
            {
                DateTime = model.EndDateTime,
                TimeZone = TimeZone
            };

            var availabilityViewInterval = model.AvailabilityViewInterval;
            return await graphService.Me.Calendar
                        .GetSchedule(schedules, endTime, startTime, availabilityViewInterval)
                        .Request()
                        .Header("Prefer", "outlook.timezone=\""+TimeZone+"\"")
                        .PostAsync();
        }
        //Api to Get Drive 
        [Route("user/drive")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserDrive()
        {
            try
            {
                var resp = await GetUserDriveWithToken();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get Drive
        public static async Task<List<UserDriveModel>> GetUserDriveWithToken()
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization = 
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var driveItems = await graphService.Me.Drive.Root.Children
                .Request()
                .GetAsync();
            List<UserDriveModel> driveList = new List<UserDriveModel>();
            foreach(var items in driveItems)
            {
                UserDriveModel model = new UserDriveModel();
                model.Id = items.Id;
                model.Ctag = items.CTag;
                model.Etag = items.ETag;
                model.CreatedDate = items.CreatedDateTime;
                model.CreatedBy = items.CreatedBy.User.DisplayName;
                model.LastModifiedDate = items.LastModifiedDateTime;
                model.LastModifiedBy = items.LastModifiedBy.User.DisplayName;
                model.ChildCount = items.Folder != null ? items.Folder.ChildCount : 0 ;
                model.FolderName = items.Name;
                model.WebUrl = items.WebUrl;
                model.Size = items.Size;
                model.IsFolder = items.Folder != null ? true : false;
                model.DriveId = items.ParentReference.DriveId;
                model.DriveItemId = items.ParentReference.Id;
                driveList.Add(model);
            }
            return driveList;
        }
        //Api to Get All Emails
        [Route("user/emails/{foldername}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserEmails(string foldername)
        {
            try
            {
                var resp = await GetUserEmailsWithToken(foldername);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                throw new ArgumentOutOfRangeException(
                    "Index Get Inbox Mails.", ex);
            }
        }
        //Get All Mails
        public static async Task<List<UserEmailModel>> GetUserEmailsWithToken(string foldername)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
             var mailList = await graphService.Me
                .MailFolders[foldername]
                .Messages
                .Request()
                .Select("Sender,Subject,ReceivedDateTime,IsRead")
                .Top(25)
                .OrderBy("ReceivedDateTime DESC")
                .GetAsync();


            List<UserEmailModel> inboxEmails = new List<UserEmailModel>();

            foreach (var mail in mailList)
            {
                UserEmailModel model = new UserEmailModel();
                model.Id = mail.Id;
                model.Subject = mail.Subject;
                model.ReceivedFrom = mail.Sender.EmailAddress.Address;
                model.ReceivedOn = mail.ReceivedDateTime;
                model.IsRead = mail.IsRead;
                inboxEmails.Add(model);
            }

            return inboxEmails;
        }
        //Api to get email details by Id or details of a particular email
        [Route("user/email/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserEmailById(string id)
        {
            try
            {
                var resp = await GetEmailPreviewById(id);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                throw new ArgumentOutOfRangeException(
                    "Can't get mail preview.", ex);
            }
        }
        //Get Email
        public static async Task<EmailPreviewModel> GetEmailPreviewById(string Id)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var response =  await graphService.Me.Messages[Id]
                .Request()
                .Header("Prefer", "outlook.body-content-type=\"html\"")
                //.Select("subject,body,bodyPreview,uniqueBody,sender,recipient")
                .GetAsync();
            EmailPreviewModel model = new EmailPreviewModel();
            model.Id = response.Id;
            model.Subject = response.Subject;
            model.ReceivedFrom = response.Sender.EmailAddress.Address;
            model.Body = response.Body.Content;
            var toRecipients = new List<Recipient>();
            var ccRecipients = new List<Recipient>();
            var bccRecipients = new List<Recipient>();
            if (response.ToRecipients != null)
            {
                foreach (var item in response.ToRecipients)
                {
                    var toRecipient = new Recipient();
                    if (toRecipient.EmailAddress == null)
                    {
                        toRecipient.EmailAddress = new EmailAddress();
                    }
                    toRecipient.EmailAddress.Address = item.EmailAddress.Address;
                    toRecipients.Add(toRecipient);
                }
            }
            if (response.CcRecipients != null)
            {
                foreach (var item in response.CcRecipients)
                {
                    var ccRecipient = new Recipient();
                    if (ccRecipient.EmailAddress == null)
                    {
                        ccRecipient.EmailAddress = new EmailAddress();
                    }
                    ccRecipient.EmailAddress.Address = item.EmailAddress.Address;
                    ccRecipients.Add(ccRecipient);
                }
            }
            if (response.BccRecipients != null)
            {
                foreach (var item in response.BccRecipients)
                {
                    var bccRecipient = new Recipient();
                    if (bccRecipient.EmailAddress == null)
                    {
                        bccRecipient.EmailAddress = new EmailAddress();
                    }
                    bccRecipient.EmailAddress.Address = item.EmailAddress.Address;
                    bccRecipients.Add(bccRecipient);
                }
            }
            model.ToRecipientList = toRecipients;
            model.CcRecipientList = ccRecipients;
            model.BccRecipientList = bccRecipients;
            return model;
        }
        //Api to create new folder in drive
        [Route("create/new/folder")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateNewFolder(UserDriveModel model)
        {
            try
            {
                var resp = await CreateNewFolderInDrive(model);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Create new folder
        public static async Task<string> CreateNewFolderInDrive(UserDriveModel model)
        {
            model.FolderName = model.FolderName == null ? "New Folder" : model.FolderName;
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var driveItem = new DriveItem
            {
                Name = model.FolderName,
                Folder = new Folder
                {
                },
                AdditionalData = new Dictionary<string, object>()
                {
                    {"@microsoft.graph.conflictBehavior","fail"}
                }
            };

            try
            {
                driveItem = await graphService.Me.Drive.Items[model.DriveId].Children
                                                                .Request()
                                                                .AddAsync(driveItem);
            }
            catch (ServiceException exception)
            {
                if (exception.StatusCode == HttpStatusCode.Conflict && exception.Error.Code == "nameAlreadyExists")
                {
                    var newFolder = await graphService.Me.Drive.Items[model.DriveId]
                        .Search(driveItem.Name) // the API lets us run searches https://learn.microsoft.com/en-us/graph/api/driveitem-search?view=graph-rest-1.0&tabs=csharp
                        .Request()
                        .GetAsync();
                    // since the search is likely to return more results we should filter it further
                    driveItem = newFolder.FirstOrDefault(f => f.Folder != null && f.Name == driveItem.Name); // Just to ensure we're finding a folder, not a file with this name
                    return("Folder already exists :" + driveItem?.Id); // your ID here
                }
                else
                {
                    return "Other ServiceException";
                }
            }
            return "Folder created successfully";
        }
        //Api to Create and send a new email
        [Route("user/send/email")]
        [HttpPost]
        public async Task<IHttpActionResult> SendUserEmails(CreateEmailModel model)
        {
            try
            {
                var resp = await SendUserEmail(model);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Send an email
        public static async Task<string> SendUserEmail(CreateEmailModel model)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var toRecipients = new List<Recipient>();
            foreach (var item in model.ToEmailRecipients)
            {
                var toRecipient = new Recipient();
                if (toRecipient.EmailAddress == null)
                {
                    toRecipient.EmailAddress = new EmailAddress();
                }
                toRecipient.EmailAddress.Address = item.Address;
                toRecipients.Add(toRecipient);
            }
            var message = new Message
            {
                Subject = model.Subject,
                Body = new ItemBody
                {
                    ContentType = BodyType.Text,
                    Content = model.EmailBody,
                },
                ToRecipients = toRecipients
            };

            await graphService.Me
                .SendMail(message, null)
                .Request()
                .PostAsync();

            return "EMail Send Succesfully";
        }
        //Api to get sharepoint site root 
        [Route("get/site/root")]
        [HttpGet]
        public async Task<IHttpActionResult> GetRootSite()
        {
            try
            {
                var resp = await GetSharePointSiteRoot();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get Sharepoint root
        public static async Task<SharePointModel> GetSharePointSiteRoot()
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            SharePointModel model = new SharePointModel();
            var siteRootdata = await graphService.Sites.Root
                .Request()
                .GetAsync();
            model.DisplayName = siteRootdata.DisplayName;
            model.WebUrl = siteRootdata.WebUrl;
            model.HostName = siteRootdata.SiteCollection.Hostname;
            model.Id = siteRootdata.Id;
            model.CreateDateTime = siteRootdata.CreatedDateTime;
            model.LastModifiedDateTime = siteRootdata.LastModifiedDateTime;

            return model;
        }

        //Api to get sharepoint site root 
        [Route("search/drive/item/{searchText}")]
        [HttpGet]
        public async Task<IHttpActionResult> SearchDriveItems(string searchText)
        {
            try
            {
                var resp = await SearchDriveItem(searchText);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get Sharepoint root
        public static async Task<List<UserDriveModel>> SearchDriveItem(string searchText)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
             var driveItemsSearch = await graphService.Me.Drive.Root
                .Search(searchText)
                .Request()
                .GetAsync();

            List<UserDriveModel> driveList = new List<UserDriveModel>();
            foreach (var items in driveItemsSearch)
            {
                UserDriveModel model = new UserDriveModel();
                model.Id = items.Id;
                model.Ctag = items.CTag;
                model.Etag = items.ETag;
                model.CreatedDate = items.CreatedDateTime;
                model.CreatedBy = items.CreatedBy.User.DisplayName;
                model.LastModifiedDate = items.LastModifiedDateTime;
                model.LastModifiedBy = items.LastModifiedBy.User.DisplayName;
                model.ChildCount = items.Folder != null ? items.Folder.ChildCount : 0;
                model.FolderName = items.Name;
                model.WebUrl = items.WebUrl;
                model.Size= items.Size;
                model.IsFolder = items.Folder != null ? true : false;
                model.DriveId = items.ParentReference.DriveId;
                model.DriveItemId = items.ParentReference.Id;
                driveList.Add(model);
            }
            return driveList;
        }

        //Api to Create a New Event
        [Route("create/calendar/event")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateCalendarEvents(CalendarDetailModel model)
        {
            try
            {
                var resp = await CreateCalendarEvent(model);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Create an Event using model
        public static async Task<Event> CreateCalendarEvent(CalendarDetailModel model)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var attendees = new List<Attendee>();
            foreach (var item in model.Attendees)
            {
                var attendee = new Attendee();
                if (attendee.EmailAddress == null)
                {
                    attendee.EmailAddress = new EmailAddress();
                }
                attendee.EmailAddress.Address = item.Address;
                attendees.Add(attendee);
            }
            var @event = new Event
            {
                Subject = model.Subject,
                Body = new ItemBody
                {
                    ContentType = BodyType.Html,
                    Content = model.EventBodyContent
                },
                Start = new DateTimeTimeZone
                {
                    DateTime = model.StartTime.ToString("dd/MM/yyyy hh:mm tt"),
                    TimeZone = TimeZone
                },
                End = new DateTimeTimeZone
                {
                    DateTime = model.EndTime.ToString("dd/MM/yyyy hh:mm tt"),
                    TimeZone = TimeZone
                },
                Location = new Location
                {
                    DisplayName = model.Location
                },
                Attendees = attendees,
                IsOnlineMeeting = model.MeetingType,
                OnlineMeetingProvider=OnlineMeetingProviderType.TeamsForBusiness
            };
            
            var calendarId = model.CalendarId;
            return await graphService.Me.Calendars[calendarId].Events
                .Request()
                .AddAsync(@event);
        }

        //Api to Get App Role
        [Route("approle/assignment")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAppRoleAssignMent()
        {
            try
            {
                var resp = await GetAppRolesAssignMent();
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get App Role Assignment
        public static async Task<IEntitlementManagementAccessPackagesCollectionPage> GetAppRolesAssignMent()
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var appRoleAssignments =  await graphService.IdentityGovernance.EntitlementManagement.AccessPackages
            .Request()
            .GetAsync();
            return appRoleAssignments;
        }

        //Api to Get Drive List By Id 
        [Route("user/drive/{driveItemId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserDrive(string driveItemId)
        {
            try
            {
                var resp = await GetUserDriveItemById(driveItemId);
                                            
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get Drive Item By ID
        public static async Task<List<UserDriveModel>> GetUserDriveItemById(string id)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            //var driveItems = new Drive();
            var driveItems = await graphService.Me.Drive.Items[id].Children.Request().GetAsync();
            List<UserDriveModel> driveList = new List<UserDriveModel>();
            foreach (var items in driveItems)
            {
                UserDriveModel model = new UserDriveModel();
                model.Id = items.Id;
                model.Ctag = items.CTag;
                model.Etag = items.ETag;
                model.CreatedDate = items.CreatedDateTime;
                model.CreatedBy = items.CreatedBy.User.DisplayName;
                model.LastModifiedDate = items.LastModifiedDateTime;
                model.LastModifiedBy = items.LastModifiedBy.User.DisplayName;
                model.ChildCount = items.Folder != null ? items.Folder.ChildCount : 0;
                model.FolderName = items.Name;
                model.WebUrl = items.WebUrl;
                model.Size = items.Size;
                model.IsFolder = items.Folder != null ? true : false;
                model.DriveId = items.ParentReference.DriveId;
                model.DriveItemId = items.ParentReference.Id;
                driveList.Add(model);
            }
            return driveList;
        }

        //Get Drive By Id
        public static async Task<List<UserDriveModel>> GetUserDriveById(string driveID,string driveItemId)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            //var driveItems = new Drive();
            var driveItems = await graphService.Drives[driveID].Items[driveItemId].Children.Request().GetAsync();
            List<UserDriveModel> driveList = new List<UserDriveModel>();
            foreach (var items in driveItems)
            {
                UserDriveModel model = new UserDriveModel();
                model.Id = items.Id;
                model.Ctag = items.CTag;
                model.Etag = items.ETag;
                model.CreatedDate = items.CreatedDateTime;
                model.CreatedBy = items.CreatedBy.User.DisplayName;
                model.LastModifiedDate = items.LastModifiedDateTime;
                model.LastModifiedBy = items.LastModifiedBy.User.DisplayName;
                model.ChildCount = items.Folder != null ? items.Folder.ChildCount : 0;
                model.FolderName = items.Name;
                model.WebUrl = items.WebUrl;
                model.Size = items.Size;
                model.IsFolder = items.Folder != null ? true : false;
                model.DriveId = items.ParentReference.DriveId;
                model.DriveItemId = items.ParentReference.Id;
                driveList.Add(model);
            }
            return driveList;
        }

        //Api to Upload file to One Drive 
        [Route("upload/file")]
        [HttpPost]
        public async Task<IHttpActionResult> UplodFiletoDrive(UserDriveModel model)
        {
            try
            {
                var resp = await UploadBigFile(model);

                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //To upload a new File in the OneDrive till 4 MB
        public static async Task<string> UploadFile(UserDriveModel model)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );

            try
            {
                //Base Path, Name and File stream of the file to upload 
                string path = model.FileBasePath;
                var filepath = Path.Combine(path, model.FileName);
                FileStream fileStream = new FileStream(filepath,FileMode.Open);
                //Call api to upload file             
               var uploadFile = await graphService.Me.Drive.Items[model.DriveId]
                                                    .ItemWithPath(model.FileName)
                                                    .Content
                                                    .Request()
                                                    .PutAsync<DriveItem>(fileStream);
                                                    
                return "File successfully upload to : " +  uploadFile.WebUrl;
            }
            catch (ServiceException ex)
            {
                return ("Failed to upload file to onedrive.");
            }
        }
        //To update the content of the existing file in the OneDrive
        public static async Task<string>UpdateFile(UserDriveModel model)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );

            try
            {
                string path = model.FileBasePath;
                byte[] data = System.IO.File.ReadAllBytes(path);
                Stream stream = new MemoryStream(data);
                // Line that updates the existing file             
                var updateFile = await graphService.Me.Drives[model.DriveId]
                                                    .Items[model.DriveItemId]
                                                    .Content.Request()
                                                    .PutAsync<DriveItem>(stream);

                return "File successfully updated to : " + updateFile;
            }
            catch (ServiceException ex)
            {
                return  ("Failed to update file.");
            }
        }

        //To upload a new File in the OneDrive Big in size file
        public static async Task<string> UploadBigFile(UserDriveModel model)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            //Base Path, Name and File stream of the file to upload 
            string path = model.FileBasePath;
            var filepath = Path.Combine(path, model.FileName);
            FileStream fileStream = new FileStream(filepath, FileMode.Open);

            // Use properties to specify the conflict behavior
            // in this case, replace
            var uploadProps = new DriveItemUploadableProperties
            {
                AdditionalData = new Dictionary<string, object>
                {
                    { "@microsoft.graph.conflictBehavior", "replace" }
                }
            };
            // Create the upload session
            // itemPath does not need to be a path to an existing item
            var uploadSession = await graphService.Me.Drive.Items[model.DriveId]
                .ItemWithPath(model.FileName)
                .CreateUploadSession(uploadProps)
                .Request()
                .PostAsync();

            var totalLength = fileStream.Length;

            //Create Upload Task
            var maxChunkSize = 230 * 1024;
            var largeUploadTask = new LargeFileUploadTask<DriveItem>(uploadSession, fileStream, maxChunkSize);
            // Create a callback that is invoked after each slice is uploaded
            IProgress<long> progress = new Progress<long>(prog => {
                ($"Uploaded {prog} bytes of {totalLength} bytes").ToString();
            });

            UploadResult<DriveItem> uploadResult = largeUploadTask.UploadAsync(progress).Result;
            if(uploadResult.UploadSucceeded)
            {
                return ("File upload successfully.");
            }else
            {
                return ("Can't upload file, Please try again lator.");
            }
        }

        //Api to Get Drive List By Id 
        [Route("download/file")]
        [HttpPost]
        public async Task<IHttpActionResult> DownloadFileFromDrive(UserDriveModel model)
        {
            try
            {
                var resp = await UploadBigFile(model);

                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }

        //To upload a new File in the OneDrive till 4 MB
        public static async Task<string> DownloadFile(UserDriveModel model)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );

            try
            {
                //Base Path, Name and File stream of the file to upload 
                string path = model.FileBasePath;
                var filepath = Path.Combine(path, model.FileName);
                FileStream fileStream = new FileStream(filepath, FileMode.Open);
                //Call api to upload file             
                var uploadFile = await graphService.Me.Drive.Items[model.DriveId]
                                                     .ItemWithPath(model.FileName)
                                                     .Content
                                                     .Request()
                                                     .PutAsync<DriveItem>(fileStream);

                return "File successfully upload to : " + uploadFile.WebUrl;
            }
            catch (ServiceException ex)
            {
                return ("Failed to upload file to onedrive.");
            }
        }

        //Api to Get all Event list from calendars
        [Route("calendar/events/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserCalendarEventsById(string id)
        {
            try
            {
                var resp = await GetEventsById(id);
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }
        }
        //Get all events from calendar
        public static async Task<CalendarDetailModel> GetEventsById(string id)
        {
            var result = await GetToken();
            GraphServiceClient graphService =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {
                    requestMessage.Headers.Authorization =
                        new AuthenticationHeaderValue("Bearer", result.ToString());
                })
            );
            var calendar = await graphService.Me.Calendar.Events[id]
                            .Request()
                            .GetAsync();

            var attendees = new List<Attendee>();
            CalendarDetailModel calendarModel = new CalendarDetailModel();
            calendarModel.EventBodyContent = calendar.Body.Content;
            calendarModel.StartTime = Convert.ToDateTime(calendar.Start.DateTime);
            calendarModel.EndTime = Convert.ToDateTime(calendar.End.DateTime);
            calendarModel.Location = calendar.Location.DisplayName;
            calendarModel.OrganizerName = calendar.Organizer.EmailAddress.Name;
            calendarModel.OrganizerEmailAddress = calendar.Organizer.EmailAddress.Address;
            calendarModel.Subject = calendar.Subject;
            calendarModel.Id = calendar.Id;
            foreach (var attendeeList in calendar.Attendees)
            {
                var attendee = new Attendee();
                if (attendee.EmailAddress == null)
                {
                    attendee.EmailAddress = new EmailAddress();
                }
                attendee.EmailAddress.Address = attendeeList.EmailAddress.Address;
                attendee.EmailAddress.Name = attendeeList.EmailAddress.Name;
                attendees.Add(attendee);
            }
            calendarModel.AttendeeList = attendees;
            
            return calendarModel;
        }
    }
}
