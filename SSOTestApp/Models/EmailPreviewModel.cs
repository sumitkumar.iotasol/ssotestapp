﻿using Microsoft.Graph;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSOTestApp.Models
{
    public class EmailPreviewModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Subject { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ReceivedFrom { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Body { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Recipient> ToRecipientList { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Recipient> CcRecipientList { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Recipient> BccRecipientList { get; set; }
    }
}