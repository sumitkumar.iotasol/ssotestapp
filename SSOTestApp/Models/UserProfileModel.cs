﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSOTestApp.Models
{
    public class UserProfileModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string GivenName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SurName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DisplayName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string MobilePhone { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OfficeLocation { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string JobTitle { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string UserPrincipalName { get; set; }
        
    }
}