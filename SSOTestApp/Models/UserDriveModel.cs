﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSOTestApp.Models
{
    public class UserDriveModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DriveItemId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DriveId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Ctag { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Etag { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CreatedDate { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? LastModifiedDate { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CreatedBy { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LastModifiedBy { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ChildCount { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long? Size { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FolderName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string WebUrl { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsFolder { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FileName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FileBasePath { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FilePath { get; set; }

    }
}