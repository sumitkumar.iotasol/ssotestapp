﻿using Microsoft.Graph;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSOTestApp.Models
{
    public class CreateEmailModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SenderId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Subject { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EmailBody { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<EmailAddress> ToEmailRecipients { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<EmailAddress> CcEmailRecipients { get; set; }
    }
}