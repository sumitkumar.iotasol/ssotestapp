﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSOTestApp.Models
{
    public class SharePointModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DisplayName { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string WebUrl { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string HostName { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CreateDateTime { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? LastModifiedDateTime { get;set; }
    }
}