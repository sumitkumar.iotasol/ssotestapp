﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSOTestApp.Models
{
    public class BearerTokenModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string UserPrincipleName { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string access_token { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string token_type { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string scope { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string expires_in { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string expires_on { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string refresh_token { get;set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? tokenexpireson { get;set; }
    }
}