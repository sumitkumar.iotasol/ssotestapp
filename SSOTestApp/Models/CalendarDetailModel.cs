﻿using Microsoft.Graph;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSOTestApp.Models
{
    public class CalendarDetailModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FirstName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LastName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DisplayName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime StartTime { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime EndTime { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Subject { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EventBodyContent { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Location { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? MeetingType { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<UserProfileModel> UsersId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? AvailabilityViewInterval { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Attendee> AttendeeList { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<EmailAddress> Attendees { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CalendarId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CalendarName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OrganizerName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OrganizerEmailAddress { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Owner { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
    }
}