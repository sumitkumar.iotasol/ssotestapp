# Thanks for installing the miniOrange ASP.NET SAML SSO library! This library will help you with integrating SAML SSO (login) for your ASP.NET (Webforms, MVC) web applications.

You can follow the below instructions to configure the SAML SSO in your ASP.NET web application!

1. This library provides you with a user dashboard for configurations. You can open this admin dashboard using the given URL : https://<your-application-base-url>/?ssoaction=config.
e.g. https://localhost:443**/?ssoaction=config, https://example.com?ssoaction=config

2. To complete the remaining configurations, the below guide will be helpful for you: 
   https://plugins.miniorange.com/asp-net-saml-sso-setup-guides

In case you require any help with setup, feel free to reach out to us at aspnetsupport@xecurify.com.

**We have an experienced technical support team who can definitely assist you with your SSO requirements!